//
//  ComicTest.swift
//  DesafioIOSMarcioMigueisTests
//
//  Created by Admin on 25/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import XCTest
@testable import DesafioIOSMarcioMigueis

class ComicTest: XCTestCase {
  
  let price1 = Price(type: "online", price: 9.99)
  let price2 = Price(type: "book", price: 10.00)
  let price3 = Price(type: "ebook", price: 7.00)

  var comic:Comic?
  
  override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    comic = Comic(id:1, title: "Hulk", description: "Big green man",thumbnail: ["image":"http://image.com"], prices : [price1, price2, price3] )
    
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
  
  func testMostExpensiveComic(){
    let mostExpensive = comic!.mostExpensive()
    print(mostExpensive.type)
    print(mostExpensive.price)
    XCTAssertEqual(mostExpensive.price, 10.0)
    XCTAssertEqual(mostExpensive.type, "book")
  }

}
