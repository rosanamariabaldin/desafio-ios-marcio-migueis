//
//  UrlableTest.swift
//  DesafioIOSMarcioMigueisTests
//
//  Created by Admin on 25/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import XCTest
@testable import DesafioIOSMarcioMigueis

class UrlableTest: XCTestCase {

  var character:Character = Character(id: 9, name: "Super T", description: "Supert T is a fantastic hero test", thumbnail: ["path": "http://image.com", "extension": "jpg"])
  
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
  
  func testUrlImageWithoutSize() {
     let url = character.urlImage()
     XCTAssertEqual(url, "https://image.com/portrait_small.jpg")
   }

   func testUrlImageWithSize() {
     let url = character.urlImage(size: .medium)
     XCTAssertEqual(url, "https://image.com/portrait_medium.jpg")
   }


}
