//
//  APIAupportTests.swift
//  DesafioIOSMarcioMigueisTests
//
//  Created by Admin on 21/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import XCTest
@testable import DesafioIOSMarcioMigueis

class APISupportTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
      
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    

  func testSecurePath(){
    let urlString = APISupport.securePath("http://test.com")
    XCTAssertEqual(urlString , "https://test.com")
  }
  
  func testSecurePathInvalid(){
    let urlString = APISupport.securePath("test.com")
    XCTAssertEqual(urlString , "")
  }
  
  func testSecurePathHasPrefixHTTP(){
    let urlString = APISupport.securePath("httptest.com")
    XCTAssertEqual(urlString , "")

  }
}
