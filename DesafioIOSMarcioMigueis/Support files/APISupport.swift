//
//  APISupport.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 21/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation

struct APISupport{
  
  static func securePath(_ path:String) -> String {
    guard path.hasPrefix("http:") || path.hasPrefix("https:") else {
      return ""
    }
    if path.hasPrefix("http://") {
      let range = path.range(of: "http://")
      var newPath = path
      newPath.removeSubrange(range!)
      return "https://" + newPath
    } else {
      return path
    }
  }
  
  static func authorizationParams() -> String {
     let publicKey   = "ec0c5ae3a87e5271003aa88c1a620788"
     let privateKey  = "c0853c24804c3fefdb75c74cc9a2483fd111bdad"
     
     let timestamp = String(NSDate().timeIntervalSince1970)
     let md5 = MD5Creator.md5Hash(str: timestamp + privateKey + publicKey)
     guard md5 != "" else { return ""}
     
     return "ts=\(timestamp)&apikey=\(publicKey)&hash=\(md5)"
   }
}
