//
//  APIReturn.swift
//  desafio-ios-marcio-migueis
//
//  Created by Admin on 21/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation


class APIReturn: Codable {
  var code: Int
  var status: String
  
  
  init(code:Int, status: String){
    self.code = code
    self.status = status
  }
}
