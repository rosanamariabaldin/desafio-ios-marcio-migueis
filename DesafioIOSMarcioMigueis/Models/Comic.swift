//
//  Comic.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 22/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation

struct Comic: Codable, Urlable {
  var id:Int
  var title:String
  var description:String?
  var thumbnail: Dictionary<String, String>
  var prices: [Price]
  
  func mostExpensive() -> Price {
    return prices.max{ $0.price < $1.price }!
  }
  
}
