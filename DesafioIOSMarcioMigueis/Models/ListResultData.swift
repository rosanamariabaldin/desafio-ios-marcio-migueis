//
//  ResultData.swift
//  desafio-ios-marcio-migueis
//
//  Created by Admin on 21/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation

struct ListResultData: Codable {
  var results = [Character]()
}
