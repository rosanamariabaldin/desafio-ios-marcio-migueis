//
//  ImageSize.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 22/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation

enum ImageSize:String {
  case small = "portrait_small"
  case medium = "portrait_medium"
  case xlarge = "portrait_xlarge"
  case uncanny = "portrait_uncanny"
}
