//
//  Price.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 23/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation

struct Price: Codable{
  var type: String
  var price: Double
}

