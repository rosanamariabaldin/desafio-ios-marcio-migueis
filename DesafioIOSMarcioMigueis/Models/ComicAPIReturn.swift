//
//  ComicAPIReturn.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 22/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation

class ComicAPIReturn: Codable {
  var code: Int
  var status: String
  var data: ComicResultData
  
}
