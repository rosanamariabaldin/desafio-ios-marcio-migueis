//
//  Character.swift
//  desafio-ios-marcio-migueis
//
//  Created by Admin on 21/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation
import UIKit

struct Character: Codable, Identifiable, Urlable {
  var id: Int
  var name: String
  var description: String?
  var thumbnail: Dictionary<String, String>
  
  
}


