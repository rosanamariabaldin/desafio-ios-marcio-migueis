//
//  ListAPIReturn.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 24/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation

class ListAPIReturn:  Codable {
   var code: Int
   var status: String
   var data: ListResultData
}
