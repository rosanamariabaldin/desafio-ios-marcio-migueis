//
//  ComicVM.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 22/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation
import RxSwift

class ComicVM {
  
  var character:Character?
  var comic:Comic?
  var comicList = [Comic]()
  
  func fetch() -> Observable<APIReturn> {
    return Observable.create{ observer in
      let urlString = "https://gateway.marvel.com:443/v1/public/characters/\(self.character?.id ?? 0)/comics?" + APISupport.authorizationParams()
      if let url = URL(string: urlString)  {
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { data, response, error in
          if let data = data {
            let decoder = JSONDecoder()
            DispatchQueue.main.async {
              if let result = try? decoder.decode(ComicAPIReturn.self, from: data) {
                self.comic = result.data.results.first
                observer.onNext(APIReturn(code: 200, status: ""))
              }else {
                if let statusReturn = try? decoder.decode(APIReturn.self, from: data) {
                  observer.onNext(statusReturn)
                }else {
                  observer.onNext(APIReturn(code: 404, status: "We can't get data"))
                }
              }
            }
          }else {
            observer.onNext(APIReturn(code: 404, status: "Invalid URL."))
          }
        }.resume()
      }
      return Disposables.create()
    }
  }
  
}
