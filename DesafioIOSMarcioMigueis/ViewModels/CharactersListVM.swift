//
//  CharactersListVM.swift
//  desafio-ios-marcio-migueis
//
//  Created by Admin on 21/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation
import RxSwift

class CharactersListVM {
  
  var listOfCharacters = [Character]()
  var limit            = 20
  var page             = 0
  var offset:Int {
    get {
      page * limit
    }
  }
  
  func fetch() -> Observable<APIReturn> {
    return Observable.create{ observer in
      let urlString = "https://gateway.marvel.com:443/v1/public/characters?limit=\(self.limit)&offset=\(self.offset)&" + APISupport.authorizationParams()
      if let url = URL(string: urlString)  {
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { data, response, error in
          if let data = data {
            let decoder = JSONDecoder()
            DispatchQueue.main.async {
              if let result = try? decoder.decode(ListAPIReturn.self, from: data) {
                self.listOfCharacters = result.data.results
                observer.onNext(APIReturn(code: 200, status: ""))
              }else {
                if let statusReturn = try? decoder.decode(APIReturn.self, from: data) {
                  observer.onNext(statusReturn)
                }else {
                  observer.onNext(APIReturn(code: 404, status: "We can't get data"))
                }
              }
            }
          }else {
            observer.onNext(APIReturn(code: 404, status: "Invalid URL."))
          }
        }.resume()
      }
      return Disposables.create()
    }
  }
  
  
}
