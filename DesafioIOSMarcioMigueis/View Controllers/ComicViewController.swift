//
//  ComicViewController.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 22/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import UIKit
import RxSwift

class ComicViewController: UIViewController, TradeMarkable, Alertable {
  
  @IBOutlet weak var comicImage: UIImageView!
  @IBOutlet weak var comicTitle: UILabel!
  @IBOutlet weak var comicPrice: UILabel!
  @IBOutlet weak var comicDescription: UILabel!
  
  @IBOutlet weak var comicType: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var imageSpinner: UIImageView!
  
  
  var character:Character?
  var comicVM    = ComicVM()
  var disposeBag = DisposeBag()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    addTradeMarkLabelToView(view: self.view)
    DispatchQueue.main.async {
      self.imageSpinner.rotate()
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    nameLabel.text = character?.name
    comicVM.character = self.character
    getData()
  }
  
  func getData() {
    let _ = comicVM.fetch()
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: {result in
        self.imageSpinner.isHidden = true
        switch  result.code {
        case 200:
          self.setupView()
        case 409:
          self.showErrorAlert(message: result.status)
        default:
          self.showErrorAlert()
        }
      })
      .disposed(by: disposeBag)
  }
  
  private func setupView(){
    if comicVM.comic != nil {
      comicTitle.text = comicVM.comic?.title
      comicImage.downloadImageFrom(link: (comicVM.comic?.urlImage(size: .xlarge))!, contentMode: UIView.ContentMode.scaleAspectFit)
      comicDescription.text = comicVM.comic?.description
      comicType.text  = comicVM.comic!.mostExpensive().type
      comicPrice.text = "$\(comicVM.comic!.mostExpensive().price)"
    }
  }
  
}
