//
//  CharacterDetailViewController.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 22/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import UIKit

class CharacterDetailViewController: UIViewController, TradeMarkable {
  
  @IBOutlet weak var characterImage: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var comicSpinner: UIImageView!
  
  var character: Character?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addTradeMarkLabelToView(view: self.view)
    DispatchQueue.main.async {
         self.comicSpinner.rotate()
       }
  }
  
  
  override func viewDidAppear(_ animated: Bool) {
    guard character != nil else { return }

    nameLabel.text = character?.name
    characterImage.downloadImageFrom(link: character!.urlImage(size: .uncanny), contentMode: UIView.ContentMode.scaleAspectFit)
    descriptionLabel.text = character?.description ?? ""
  }
  
  
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   if segue.identifier == "comic"
   {
     if let destinationVC = segue.destination as? ComicViewController {
       destinationVC.character = character
     }
   }
  }
   
  
}
