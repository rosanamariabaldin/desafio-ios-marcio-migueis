//
//  ViewController.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 21/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import UIKit
import RxSwift

class CharacterListViewController: UIViewController, TradeMarkable, Alertable {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var imageSpinner: UIImageView!
  
  var charactersListVM = CharactersListVM()
  var selectedCharacter: Character?
  
  let disposeBag = DisposeBag()
  var spinnerLabel = UILabel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    tableView.register(UINib(nibName: "CharacterListTableViewCell", bundle: nil), forCellReuseIdentifier: "characterCell")
    tableView.tableFooterView = UIView()
    addTradeMarkLabelToView(view: self.view)
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "next 20", style: .plain, target: self, action: #selector(seeMoreData(sender:)))

  }
  
  
  override func viewDidAppear(_ animated: Bool) {
    getData()
    DispatchQueue.main.async {
      self.imageSpinner.rotate()
    }
  }
  
  @objc func seeMoreData(sender: UIBarButtonItem){
    guard sender.title != nil else { return }
    if sender.title!.contains("next"){
      self.imageSpinner.isHidden = false
      charactersListVM.page += 1
      getData()
    }else {
      if charactersListVM.page > 0  {
        self.imageSpinner.isHidden = false
        charactersListVM.page -= 1
        getData()
      }
    }
    tableView.setContentOffset(.zero, animated: true)

    navigationItem.leftBarButtonItem = charactersListVM.page > 0 ? UIBarButtonItem(title: "previous 20", style: .plain, target: self, action: #selector(seeMoreData(sender:))) : nil
  }
  
  func getData() {
    let _ = charactersListVM.fetch()
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: {result in
        self.imageSpinner.isHidden = true
        switch  result.code {
        case 200:
           self.tableView.reloadData()
        case 409:
          self.showErrorAlert(message: result.status)
        default:
          self.showErrorAlert()
        }
      })
      .disposed(by: disposeBag)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showDetail" {
      if let destinationVC = segue.destination as? CharacterDetailViewController {
        destinationVC.character = selectedCharacter
      }
    }
  }
  
}

extension CharacterListViewController: UITableViewDelegate{
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    selectedCharacter = charactersListVM.listOfCharacters[indexPath.row]
    performSegue(withIdentifier: "showDetail", sender: nil)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 80.0
  }
}

extension CharacterListViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return charactersListVM.listOfCharacters.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "characterCell", for: indexPath) as! CharacterListTableViewCell
    
    let character  = charactersListVM.listOfCharacters[indexPath.row]
    cell.setup(character: character)
    
    return cell
    
  }
  
}

extension UIImageView{
    func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
}
