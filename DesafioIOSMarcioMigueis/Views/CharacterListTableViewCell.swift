//
//  CharacterListTableViewCell.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 21/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import UIKit

class CharacterListTableViewCell: UITableViewCell {
  
  @IBOutlet weak var picture: UIImageView!
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  
  func setup(character: Character){
    name.text = character.name
    picture.isHidden = true
    picture.downloadImageFrom(link: character.urlImage(), contentMode: UIView.ContentMode.scaleAspectFit)
    picture.isHidden = false
    
  }
}


extension UIImageView {
  func downloadImageFrom(link:String, contentMode: UIView.ContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}
