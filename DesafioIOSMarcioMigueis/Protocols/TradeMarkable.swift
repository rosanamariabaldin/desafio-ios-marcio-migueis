//
//  AddTradeMarkProtocol.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 22/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import UIKit

protocol TradeMarkable {
  func addTradeMarkLabelToView(view: UIView)
}

extension TradeMarkable {
  
  func addTradeMarkLabelToView(view: UIView)  {
    let label = UILabel(frame: CGRect(x: 10, y: view.frame.height - 37, width: view.frame.width - 30, height: 30))
    label.text = "Data provided by Marvel. © 2014 Marvel"
    label.textColor = .black
    label.textAlignment = .right
    label.font = UIFont.systemFont(ofSize: 11)
    view.addSubview(label)
  }
}

