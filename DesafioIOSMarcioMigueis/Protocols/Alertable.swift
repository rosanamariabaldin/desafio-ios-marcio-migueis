//
//  Alertable.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 23/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import UIKit

protocol Alertable: UIViewController {
  func showErrorAlert(message: String)
}

extension Alertable {
  func showErrorAlert(message: String = "Sorry, something went wrong. "){
    let fullMessage = message + "\nTry later please."
    let alert = UIAlertController(title: "Ops", message: fullMessage, preferredStyle: UIAlertController.Style.alert)
    
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
    }))
    self.present(alert, animated: true, completion: nil)
  }
}
