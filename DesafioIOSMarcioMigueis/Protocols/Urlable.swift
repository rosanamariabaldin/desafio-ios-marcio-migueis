//
//  Urlable.swift
//  DesafioIOSMarcioMigueis
//
//  Created by Admin on 25/05/20.
//  Copyright © 2020 ar1 mobile. All rights reserved.
//

import Foundation

protocol Urlable{
  var thumbnail: Dictionary<String, String> { get set }
   func urlImage(size: ImageSize) -> String
}


extension Urlable {
  func urlImage(size: ImageSize = .small) -> String {
    return APISupport.securePath(path) + "/" + size.rawValue + "." + typeOfImage
  }
  
  private var typeOfImage:String {
    get {
      thumbnail["extension"] ?? ""
    }
  }
  private var path:String {
    get{
      thumbnail["path"] ?? ""
    }
  }
}
