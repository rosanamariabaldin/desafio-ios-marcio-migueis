# Desafio-ios-marcio-migueis

Código do desafio para vaga de desenvolvedor utilizando API Marvel.

## Arquitetura

Optei por fazer um MVVM simples . Criei alguns protocolos e alguns arquivos de suporte.

### Pods

Utilizei o RxSwift para tratar de forma assincrona as chamadas à API. Como a API retornava o resultado com os dados ou ao menos o status e uma mensagem de erro. Criei um model APIReturn que nos casos de erro da API, trazia o status e a mensagem de erro. O status era sempre 409 mas a mensagem varia. Ao receber erro, o viewcontroller mostra esta mensagem.

### Instruções

É preciso clocar o repositório e instalar os pods existentes

### testes unitários

Fiz somente os testes de models ( o que quer dizer que não fiz testes de network nem de view , perdão ).


## Layout

Procurei utilizar as cores que me pareceram correspondetes às do site, mas sem muita preocupação em estar 100% correto. 

Fiz um spinner  (activeControl) personalizado que me pareceu bem bacana e apropriado.

Não fiz animação entre views porque não pensei em nada muito diferente.

Cuidei de criar um ícone e um splash_screen para o projeto.

Coloquei a mensagem de copyright conforme a API Marvel solicitava.

## Possíveis melhorias

Pensei em criar um cache com os 20 anteriores e os próximos vinte. Assim a nagevação ficaria mais rápida para o usuário. Não era requisito mas se o app fosse  "de verdade" eu teria criado. A ideia é , ao buscar os 20 personagens , disparar outras duas threads para buscar os vinte anteriore e os próximoss vinte ( em background ) , e salvar em dois arrays correspondentes. Quando o usuário clicasse na paginação, estes arrays auxiliares seriam mostrados imediatamente e uma nova busca em background iniciaria. 

Faltou criar um layout para os botões de paginação ( 20 anteriores e próximos 20 ) 

## Demais considerações

Eu iniciei o projeto usando Combine e SwitUI, mas tive problemas de performance ao buscar as imagens . Não descobri o motivo e resolvi reescrever tudo em UIKit mesmo, usando RxSwift e storyboard.
